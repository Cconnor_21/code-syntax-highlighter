var tag, tagText, equals, value, attribute, ident, plain;

function setColors(t, tt, e, v, a, i, p){
	console.log(t);
	tag = t;
	tagText = tt;
	equals = e;
	value = v;
	attribute = a;
	ident = i;
	plain = p;
}

function stringToArr(str){
	var arr = [];
	[...str].forEach(function(char){
		if(char !== " "){
			arr.push(char);
		}
	});
	return arr;
}

function arrayToString(arr){
	var string = "";
	arr.forEach(function(item){
		string += item;
	});
	return string;
}

function convertHTML(str){
	var arr = [];
	[...str].forEach(function(char){
		if(char === "<")
		{
			arr.push("&lt;");
		}
		else if(char === ">")
		{
			arr.push("&gt;");
		}
		else{
			arr.push(char);
		}
	});
	return arrayToString(arr);
}

//TODO fix space issue in attribute value
function addSpanToTag(str){
	var arr = [];
	var insideQ = false;
	[...str].forEach(function(char){
		if(char === "<"){
			arr.push(`<span style="color:${tag}">${convertHTML(char)}</span><span style="color:${tagText}">`);
		}
		else if(char === ">"){
			arr.push(`</span><span style="color:${tag}">${convertHTML(char)}</span>`);
		}
		else if(char === "/" && insideQ == false){
			arr.push(`<span style="color:${tagText}"><span class='tag'>${convertHTML(char)}</span>`);
		}
		else if(char === " " && insideQ == true)
		{
			arr.push(`&nbsp;<span style="color:${value}">`);
		}
		else if(char === " " && insideQ == false){
			arr.push(`&nbsp;<span style="color:${attribute}">`);
		}
		else if(char === `"` && insideQ == true){
			insideQ = false;
			arr.push(`"<span style="color:${attribute}">`);
		}
		else if(char === `"` && insideQ == false){
			insideQ = true;
			arr.push(`<span style="color:${value}">"`);
		}
		else if(char === "="){
			arr.push(`</span><span style="color:${equals}">${convertHTML(char)}</span>`);
		}
		else{
			arr.push(char);
		}
	});
	return arrayToString(arr);
}

function plainText(arr){
	var a = [];
	var string = "";
	arr.forEach(function (item){
		if(item.length > 1)
		{
			if(string.length > 1)
			{
				a.push(string);
			}
			string = "";
			a.push(item);
		}
		else{
			string += item.toString();
		}
	});
	return a;
}

function highlightID(arr){
	var a = [];
	arr.forEach(function(item){
		if(item.includes("id"))
		{
			var i = item.replace("id", `<span style="color:${ident}">id</span>`);
			a.push(i);
		}
		else{
			a.push(item);
		}
	});
	return a;
}

function stylePlain(arr){
	var a = [];
	arr.forEach(function(item){
		if(!item.includes("span"))
		{
			a.push(item.replace(item, `<span style="color:${plain}">${item}</span>`));
		}
		else{
			a.push(item);
		}
	});
	return a;
}

function highlightHTML(str){
var arr = [];
var count = 0;
var startTag, endTag, startPlain, endPlain;
var n = str;
var insideTag = false;

[...n].forEach(function (char){
	if(char === "<"){
		insideTag = true;
		startTag = count;
		var tag = n.substring(startTag, endTag);
		count++;
	}
	else if(char === ">"){
		endTag = count + 1;
		var tag = n.substring(startTag, endTag);
		arr.push(addSpanToTag(tag));
		insideTag = false;
		count ++;
	}
	else if(insideTag === false){
			arr.push(char);
			count++;
	}
	else{
		count++;
	}
});
	arr = plainText(arr);
	arr = stylePlain(arr);
	arr = highlightID(arr);
  return arrayToString(arr);
}
